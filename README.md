## MongoDB Cheat Sheet

# show all database

```
show dbs
```

# show current database

```
db
```

# Create or switch database

```
use test_db
```

# Drop database

```
db.dropDatabase()
```

# Create Collection

```
db.createCollection('posts')
```

#Show Collection

```
show collections
```

# Insert Row

```
db.posts.insert([
       {
            title:"First post",
            body:"My first post",
            category:"Science",
            comments:[
                   {
                    comment_id:1234,
                    comment_title:"Technolgy"
                 },
                    {
                    comment_id:1235,
                    comment_title:"Space"
                    }
           ],
           tags:["spaceExplore","spaceMission"],
           date:Date(),
             subsection:{
                     id:121,
                     title:"subpost inside post1"
                    }
        }
 ])
```

# Insert multiple Rows

```
db.posts.insertMany([
       {
        title:"Second post",
        body:"My second post",
        category:"Sports",
        comments:[
            {
            comment_id:1236,
            comment_title:"Basketball"
            },
            {
            comment_id:1238,
            comment_title:"Cricket"
            }
        ],
        tags:["play","games"],
        date:Date(),
         subsection:{
                id:122,
                title:"subpost inside post2"
        }
    },
   {
       title:"Third post",
        body:"My third post",
        category:"history",
        comments:[
            {
            comment_id:1244,
            comment_title:"indian history"
            },
            {
            comment_id:1243,
            comment_title:"western history"
            }
        ],
        tags:["java","oops"],
        date:Date(),
         subsection:{
                id:123,
                title:"subpost inside post3"
        }
    }
    
])

```

# Get All Rows

```
db.posts.find()
```

# Get All Rows Formatted

```
db.find().pretty()
```

# Find Rows

```
db.find({category:"Sports"}).pretty()
```


# Sort Rows

```
# Ascending
 db.posts.find().sort({category:1}).pretty()
# Descending
db.posts.find().sort({category:-1}).pretty()
```

# Count Rows

```
db.posts.find().count()
db.posts.find({title:"Second post"}).count()

```

# Limit Rows

```
db.posts.find().limit(3).pretty()
```

# Chaining

```
db.posts.find().limit(2).sort({category:-1}).pretty()
```

# Foreach

```
db.posts.find().forEach(function(doc){print("Blog category: "+doc.category)})
```

# Find One Row

```
 db.posts.findOne({title:"First post"})
```

